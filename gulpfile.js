var gulp = require('gulp');
var plug = require('gulp-load-plugins')();
var historyApiFallback = require('connect-history-api-fallback');
var paths = require('./paths.json');
var browserSync = require('browser-sync').create();

//try to set CORS
var cors = function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'aaaddsd');
    res.setHeader('Access-Control-Allow-Headers', 'asdasdsad');
    next();
};
/**
 * @desc Create, connect to the server
 */
var reload = browserSync.reload;
gulp.task('connect', ['watch'], function () {
    browserSync.init({
        notify: false,
        port: 3000,
        server: {
            baseDir: [
                'client/app'
            ],
            middleware: [ historyApiFallback(), cors ]
        }
    });
});

/**
 * @desc Watch files
 */
gulp.task('watch', function () {
    gulp.watch([paths.js, paths.html]).on('change', browserSync.reload);
    gulp.watch(paths.scss, ['sass']);
});

/**
 * @desc Create $templateCache from the html templates
 */
gulp.task('templatecache', function () {

    return gulp
        .src(paths.html)
        .pipe(plug.angularTemplatecache('templates.js', {
            module: 'app',
            standalone: false,
            root: ''
        }))
        .pipe(gulp.dest(paths.build));
});

/**
 * @desc Watch scss files and compile
 */
gulp.task('sass', function () {
    return gulp.src(paths.client + 'main.scss')
        .pipe(plug.sass().on('error', plug.sass.logError))
        .pipe(gulp.dest(paths.client + 'content/css'))
        .pipe(reload({stream: true}));
});

/**
 * @desc Minify and bundle the CSS
 */
gulp.task('css', ['sass'], function () {
    return gulp.src(paths.css)
        .pipe(plug.concat('all.min.css'))
        .pipe(plug.autoprefixer('last 2 version', '> 5%'))
        .pipe(plug.minifyCss({}))
        .pipe(gulp.dest(paths.build + 'content/css'));
});

/**
 * @desc Minify and bundle the Vendor CSS
 */
gulp.task('vendorcss', function () {
    return gulp.src(paths.vendorcss)
        .pipe(plug.concat('vendor.min.css'))
        .pipe(plug.minifyCss({}))
        .pipe(gulp.dest(paths.build + 'content/css'));
});

/**
 * @desc Minify and bundle the app's JavaScript
 */
gulp.task('js', ['templatecache'], function () {

    var source = [].concat(paths.js);

    return gulp
        .src(source)
        .pipe(plug.concat('all.min.js'))
        .pipe(plug.ngAnnotate({add: true, single_quotes: true}))
        .pipe(plug.uglify({mangle: true}))
        .pipe(gulp.dest(paths.build));
});

/**
 * @desc Copy the Vendor JavaScript
 */
gulp.task('vendorjs', function () {
    return gulp.src(paths.vendorjs)
        .pipe(plug.concat('vendor.min.js'))
        .pipe(plug.uglify())
        .pipe(gulp.dest(paths.build));
});


/**
 * @desc Copy fonts
 */
gulp.task('fonts', function () {
    var dest = paths.build + 'content/fonts';
    return gulp
        .src(paths.fonts)
        .pipe(gulp.dest(dest));
});

/**
 * Inject all the files into the new index.html
 * rev, but no map
 */
gulp.task('rev-and-inject', ['js', 'vendorjs', 'css', 'vendorcss', 'fonts'], function () {

    var minified = paths.build + '**/*.min.*';
    var index = paths.client + 'index.html';

    var minFilter = plug.filter(['**/*.min.*', '!**/*.map']);
    var indexFilter = plug.filter(['index.html']);

    return gulp
        .src([].concat(minified, index))
        .pipe(minFilter)
        .pipe(gulp.dest(paths.build))
        .pipe(minFilter.restore())
        .pipe(indexFilter)
        .pipe(inject('content/css/vendor.min.css', 'inject-vendor'))
        .pipe(inject('content/css/all.min.css'))
        .pipe(inject('vendor.min.js', 'inject-vendor'))
        .pipe(inject('all.min.js'))
        .pipe(inject('templates.js', 'inject-templates'))
        .pipe(indexFilter.restore())
        .pipe(gulp.dest(paths.build));

    function inject(path, name) {

        var glob = paths.build + path;
        var options = {
            ignorePath: paths.build.substring(1),
            addRootSlash: false
        };
        if (name) {
            options.name = name;
        }
        return plug.inject(gulp.src(glob), options);
    }
});

/**
 * Build the optimized app
 */
gulp.task('build', ['rev-and-inject'], function () {

    return gulp.src('').pipe(plug.notify({
        onLast: true,
        message: 'Deployed code!'
    }));
});

/**
 * @desc Default gulp task
 */
gulp.task('default', ['connect']);