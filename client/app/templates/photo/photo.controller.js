;(function () {
    angular
        .module('app')
        .controller('OnePhoto', OnePhoto);

    /* @ngInject */
    function OnePhoto(initialData) {
        console.log(initialData);
        var vm = this;
        vm.photo = initialData;
        vm.choosePhoto = choosePhoto;

        function choosePhoto(width) {
            if (vm.photo.src_xbig) return (width > window.innerWidth) ? vm.photo.src_big : vm.photo.src_xbig;

            return vm.photo.src_big;
        }
    }
})();