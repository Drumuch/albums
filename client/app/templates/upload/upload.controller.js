;(function () {
    angular
        .module('app')
        .controller('Upload', Upload);

    /* @ngInject */
    function Upload($sce, $log, initialData, upload) {
        console.log('Upload controller initialize....');
        var vm = this;
        vm.albumId = '';
        vm.albums = initialData;
        vm.dzAddedFile = function (file) {
            $log.log(file);
        };

        vm.uploadUrl = '/';

        vm.dzError = function (file, errorMessage) {
            $log.log(errorMessage);
        };
        vm.prr = function (file) {
            console.log(1111);
            this.options.url = vm.uploadUrl;
        };

        vm.dropzoneConfig = {
            parallelUploads: 5,
            maxFileSize: 50,
            url: '/',
            headers: {
                //'origin': 'http://evil.com/',
                'Access-Control-Allow-Origin': '*://*/*',
                'Access-Control-Allow-Credentials': 'true',
                "Content-Type": "application/json",
                "Content-Language":"en-US",
                "Accept":"application/json;text/plain",
                "X-Requested-With": "XMLHttpRequest",
                'Access-Control-Allow-Headers': 'Content-Type, Authorization, X-Requested-With',
                'Access-Control-Max-Age': '86400'
            }
        };

        vm.onChange = function () {
            upload.getUrl(vm.albumId)
                .then(function (res) {
                    console.log(res.response);
                    vm.uploadUrl = $sce.trustAsResourceUrl(res.response.upload_url);
                })
        };

        console.log(initialData);

    }
})();