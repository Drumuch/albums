;(function () {
    angular
        .module('app')
        .controller('Albums', Albums);

    /* @ngInject */
    function Albums(initialData, albumslist, albums) {
        var vm = this,
            i,
            thumbArray = [],
            albumsL = albumslist.length;

        vm.name = initialData.first_name;
        vm.albums = albumslist;


        console.log(albumslist);

        for(i = 0; i < albumsL; i++) {
            var time = moment(vm.albums[i].updated * 1000);
            vm.albums[i].prev = 'images/loader.gif';
            vm.albums[i].lastChange = time.format('LL');
            thumbArray.push(vm.albums[i].thumb_id);
        }

        albums.getPhoto(thumbArray)
            .then(function (res) {
                console.log(res);
                for(i = 0; i < albumsL; i++) {
                    vm.albums[i].prev = res.response[i].src;
                }
            });
    }
})();