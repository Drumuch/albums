;(function () {
    angular
        .module('app')
        .controller('Album', Album);

    /* @ngInject */
    function Album(photos, album, albums, $stateParams) {
        var vm = this,
            showingTooltip,
            app = document.getElementById('photos');

        vm.photos = photos;
        vm.busy = false;
        vm.album = album.title;
        vm.iScroll = true;
        vm.lastScrollTop = 0;
        vm.imageData = {
            count: 3,
            offset: 5 // initial offset in resolve
        };

        $(document).bind('scroll', function () {
            var st = $(window).scrollTop();
            var scrollPercentage = (($(window).scrollTop() + $(window).height()) / $(document).height()) * 100;
            if (st > vm.lastScrollTop && scrollPercentage > 85 && vm.iScroll) {
                addImages();
            }
            vm.lastScrollTop = st;
        });

        app.addEventListener('mouseover', function (e) {
            if (e.target && e.target.nodeName === 'IMG') {
                showingTooltip = showTooltip(e.target);
            }
        });

        app.addEventListener('mouseout', function (e) {
            if (e.target && e.target.nodeName === 'IMG') {
                showingTooltip.style.display = 'none';
            }
        });

        function addImages() {
            if (vm.busy) { return; }
            vm.busy = true;
            vm.firstReq ? vm.imageData.offset = 5 : vm.imageData.offset += +vm.imageData.count;
            albums.getOffsetPhoto($stateParams.id, vm.imageData)
                .then(function (res) {
                    console.log(res);
                    
                    vm.firstReq = false;
                    vm.busy = false;
                    vm.photos = vm.photos.concat(res.response);
                });
        }

        function showTooltip(elem) {
            var positionY,
                positionX,
                docWidth = document.body.clientWidth,
                coords = elem.parentNode.getBoundingClientRect(),
                tooltip = elem.parentNode.nextElementSibling.children[0];

            tooltip.style.display = 'block';

            // 8 width border of image
            positionX = coords.left + 8 + tooltip.offsetWidth - docWidth;
            positionY = window.innerHeight - coords.bottom - tooltip.offsetHeight;

            if (-positionX > 0) positionX = 0;

            if (positionY > 0) {
                positionY = -tooltip.offsetHeight - 10;
                tooltip.setAttribute('data-triangle', 'top')
            } else {
                positionY = coords.bottom - coords.top + 10 ;
                tooltip.setAttribute('data-triangle', 'bottom')
            }

            tooltip.style.left = -positionX + 'px';
            tooltip.style.bottom = positionY + 'px';

            return tooltip;
        }
    }
})();