;(function () {
    'use strict';

    angular
        .module('app')
        .config(mainConfig);

    /* @ngInject */
    function mainConfig($stateProvider, $urlRouterProvider, $locationProvider) {
        $urlRouterProvider
            .when('/albums', '/albums/all');
        $urlRouterProvider
            .otherwise('/login');
        // use additional property class with name of font awesome class
        // for building tabs with icons
        $stateProvider
            .state('app', {
                abstract: true,
                template: '<tabs></tabs><ui-view></ui-view>'
            })
            .state('app.albums', {
                url: '/albums',
                class: 'fa-picture-o',
                template: '<ui-view></ui-view>'
            })
            .state('app.albums.all', {
                url: '/all',
                parent: 'app.albums',
                templateUrl: 'templates/albums/all/albums.html',
                controller: 'Albums',
                controllerAs: 'vm',
                resolve: {
                    /* @ngInject */
                    initialData: function (albums) {
                        return albums.getUser()
                            .then(function (res) {
                                return res.response[0];
                            })
                    },
                    /* @ngInject */
                    albumslist: function (albums) {
                        return albums.getAlbums()
                            .then(function (res) {
                                return res.response;
                            })
                    }
                }
            })
            .state('app.albums.one', {
                url: '/one/:id',
                parent: 'app.albums',
                templateUrl: 'templates/albums/one/album.html',
                controller: 'Album',
                controllerAs: 'vm',
                resolve: {
                    /* @ngInject */
                    photos: function ($stateParams, albums) {
                        return albums.getOffsetPhoto($stateParams.id, {count: 5, offset: 0})
                            .then(function (res) {
                                return res.response;
                            });
                    },
                    /* @ngInject */
                    album: function ($stateParams, albums) {
                        return albums.getAlbum($stateParams.id)
                            .then(function (res) {
                                return res.response[0];
                            });
                    }
                }
            })
            .state('app.albums.photo', {
                url: '/photo/:id',
                parent: 'app.albums',
                templateUrl: 'templates/photo/photo.html',
                controller: 'OnePhoto',
                controllerAs: 'vm',
                resolve: {
                    /* @ngInject */
                    initialData: function ($stateParams, albums) {
                        return albums.getPhoto([$stateParams.id])
                            .then(function (res) {
                                return res.response[0];
                            });
                    }
                }
            })
            .state('app.upload', {
                url: '/upload',
                templateUrl: 'templates/upload/upload.html',
                controller: 'Upload',
                controllerAs: 'vm',
                class: 'fa-upload',
                resolve: {
                    /* @ngInject */
                    initialData: function (albums) {
                        return albums.getAlbums()
                            .then(function (res) {
                                return res.response;
                            })
                    }
                }
            })
            .state('login', {
                url: '/login',
                templateUrl: 'templates/login/login.html',
                controller: 'Login',
                controllerAs: 'vm',
                class: 'fa-power-off'
            });
        $locationProvider.html5Mode(true);
    }
})();