;(function () {
    'use strict';

    angular
        .module('app',['app.core'])
        .run(run);

        /* @ngInject */
        function run($localStorage, $state, $timeout, $rootScope) {
            var hash = window.location.hash;


            if (hash.indexOf('access_token') !== -1) {
                $rootScope.vkScope = parseHash(hash);
                console.log($rootScope.vkScope);
                $localStorage.vkSkope = $rootScope.vkScope;
            }


            if ($localStorage.vkSkope && $localStorage.vkSkope.access_token) {
                $timeout(function() {
                    //$state.go('app.albums.all');
                })
            } else {
                $timeout(function() {
                    $state.go('login');
                })
            }

            function parseHash(aURL) {

                var result = {};
                var hashes = aURL.slice(aURL.indexOf('#') + 1).split('&');

                for(var i = 0; i < hashes.length; i++) {
                    var hash = hashes[i].split('=');

                    if(hash.length > 1) {
                        result[hash[0]] = hash[1];
                    } else {
                        result[hash[0]] = null;
                    }
                }

                return result;
            }
        }
})();