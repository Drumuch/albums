;(function () {
    'use strict';

    angular
        .module('app')
        .service('albums', albums);


    /* @ngInject */
    function albums($localStorage, http, url) {

        var userId = $localStorage.vkSkope.user_id;

        return {
            getUser: getUser,
            getAlbums: getAlbums,
            getAlbum: getAlbum,
            getPhoto: getPhoto,
            getAlbumPhotos: getAlbumPhotos,
            getOffsetPhoto: getOffsetPhoto
        };

        function getUser() {
            return http.jsonp(url.user.getUser + userId);
        }

        function getAlbums() {
            return http.jsonp(url.photo.getAlbums + userId);
        }

        function getAlbum(id) {
            return http.jsonp(url.photo.getAlbums + userId + '&album_ids=' + id);
        }

        function getPhoto(photo) {
            return http.jsonp(url.photo.getPhoto + queryString(userId, photo));
        }

        function getOffsetPhoto(id, data) {
            return http.jsonp(url.photo.getAlbumPhotos + '&owner_id=' + userId + '&album_id=' + id + '&extended=1' +
                '&count='+ data.count + '&offset='+ data.offset)
        }

        function getAlbumPhotos(id) {
            return http.jsonp(url.photo.getAlbumPhotos + '&owner_id=' + userId + '&album_id=' + id + '&extended=1')
        }

        function queryString(user, photo) {
            var i,
                str = '',
                length = photo.length;
            for (i = 0; i < length; i++) {
                str += user + '_' + photo[i] + ',';
            }
            str = str.slice(0, -1);
            return str;
        }
    }
})();