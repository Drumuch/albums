;(function () {
    'use strict';

    angular
        .module('app')
        .service('upload', upload);


    /* @ngInject */
    function upload($localStorage, http, url) {

        var userId = $localStorage.vkSkope.user_id;

        return {
            getUrl: getUrl
        };

        function getUrl(id) {
            return http.jsonp(url.upload.getUrl + id);
        }
    }
})();