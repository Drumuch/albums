;(function () {
    'use strict';
    angular
        .module('factory.request', [])
        .factory('http', http);

    /* @ngInject */
    function http($http, $q, $localStorage) {

        return {
            jsonp: function(url) { return jsonp(url)}
        };

        function jsonp(url) {
            var config = {
                method: 'JSONP',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json; charset=UTF-8'
                }
            };

            var token = $localStorage.vkSkope.access_token;

            if (token) { config.url = url + '&access_token=' + token; }
            else { config.url = url; }

            config.url += '&callback=JSON_CALLBACK';

            return $http.jsonp(config.url)
                .then(requestComplete)
                .catch(requestFailed)
        }

        function requestFailed(err) {
            console.info('error', err.config.url, err);

            if (err.data === null || !err.data.error) {
                if (err.status === 200) { console.log('Server Error: ' + err.data); }
                else if (err.status === -1) { console.log('Server unavailable'); }
                else if (err.status === 0) { console.log('No internet connection'); }
                else if (err.status === 500) { console.log('Server Error: ' + err.status + ' ' + err.data.message);}
                else { console.log('Server Error: ' + err.status + ' ' + err.statusText); }

                console.log('XHR Failed: ' + err.status);
            } else {
                console.log('Error: ' + err.data.error);
            }
            return err.data.error;
        }

        function requestComplete(response) {
            var promise = $q.defer();

            console.info('response complete', response.config.url, response);

            if (!response.data.error) { promise.resolve(response.data); }
            else { promise.reject(response); }
            return promise.promise;
        }
    }
})();