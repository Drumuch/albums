;(function () {
    'use strict';
    angular
        .module('factory.urlRequest', [])
        .factory('url', url);

    function url() {

        var baseUrl = 'https://api.vk.com/method/';
        return {
            user: {
                getUser:        baseUrl + 'users.get?user_ids='
            },
            photo: {
                getPhoto:       baseUrl + 'photos.getById?photos=',
                getAlbums:      baseUrl + 'photos.getAlbums?owner_id=',
                getAlbumPhotos: baseUrl + 'photos.get?'
            },
            upload: {
                getUrl:         baseUrl + 'photos.getUploadServer?album_id='
            }
        };
    }

})();
