;(function () {
    'use strict';
    angular
        .module('app.core', [
            /*
             * Angular modules
             */
            'ui.router',
            'ngStorage',
            'ngDropzone',
            /*
             * Our reusable cross app code modules
             */

            'factories.module'
            /*
             * 3rd Party modules
             */
        ]);
})();
