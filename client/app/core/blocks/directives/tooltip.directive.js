;(function () {
    'use strict';
    angular
        .module('app')
        .directive('tooltip', function () {
            return {
                restrict: 'AE',
                scope: {
                    likes: '=',
                    comments: '=',
                    content: '=',
                    reposts: '='
                },
                transclude: true,
                link: function (scope, element, attrs) {
                },
                templateUrl: '/core/blocks/directives/tooltip.html'
            }
        })
})();