;(function () {
    'use strict';
    angular
        .module('app')
        .directive('tabs', function ($state, $rootScope) {
            return {
                restrict: 'AE',
                transclude: true,
                controllerAs: 'vm',
                controller: function () {
                    var vm = this;
                    vm.tabs = $state.get().splice(2);
                },
                templateUrl: '/core/blocks/directives/tabs.html'
            }
        })
})();